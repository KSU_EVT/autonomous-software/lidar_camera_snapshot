#include <memory>
#include <ctime>
#include <filesystem>
#include <rclcpp/qos.hpp>
#include <string>
#include <fstream>
#include <sensor_msgs/msg/detail/compressed_image__struct.hpp>
#include <sensor_msgs/msg/detail/point_cloud2__struct.hpp>

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/msg/compressed_image.hpp>
#include <std_srvs/srv/empty.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

using std::placeholders::_1;
using std::placeholders::_2;

class LidarCameraSnapshotNode : public rclcpp::Node
{
private:
  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::PointCloud2>> pointcloud_sub_;
  sensor_msgs::msg::PointCloud2 pointcloud2_msg_;
  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::CompressedImage>> image_sub_;
  sensor_msgs::msg::CompressedImage compressed_img_msg_;
  std::shared_ptr<rclcpp::Service<std_srvs::srv::Empty>> snapshot_srv_;
  std::filesystem::path snapshot_dir_;

  uint64_t snapshot_counter_ = 0;
  //pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_; // member variable to save memory allocations

  void pointcloud_callback(const std::shared_ptr<sensor_msgs::msg::PointCloud2> msg) {
    pointcloud2_msg_ = *msg;
  }

  void image_callback(const std::shared_ptr<sensor_msgs::msg::CompressedImage> msg) {
    compressed_img_msg_ = *msg;
  }

  void save_snapshot(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                     std::shared_ptr<std_srvs::srv::Empty::Response> response) {
    (void)request;
    (void)response;

    // pcd
    auto pcd_path = snapshot_dir_ / (std::to_string(snapshot_counter_) + ".pcd");
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud =
      std::make_shared<pcl::PointCloud<pcl::PointXYZI>>();

    // Get the field structure of this point cloud
    int pointBytes = pointcloud2_msg_.point_step;
    int offset_x = 0;
    int offset_y = 4;
    int offset_z = 8;
    int offset_int = 12;

    // populate point cloud object
    for (int p = 0; p < pointcloud2_msg_.width; ++p) {
      pcl::PointXYZI newPoint;
      
      newPoint.x = *(float*)(&pointcloud2_msg_.data[0] + (pointBytes*p) + offset_x);
      newPoint.y = *(float*)(&pointcloud2_msg_.data[0] + (pointBytes*p) + offset_y);
      newPoint.z = *(float*)(&pointcloud2_msg_.data[0] + (pointBytes*p) + offset_z);
      newPoint.intensity = *(uint32_t*)(&pointcloud2_msg_.data[0] + (pointBytes*p) + offset_int);
      
      cloud->points.push_back(newPoint);
    }

    //std::cout << "cloud size: " << cloud->points.size() << "\n";

    pcl::io::savePCDFileBinary(pcd_path.string(), *cloud);

    // image
    auto image_path = snapshot_dir_ / (std::to_string(snapshot_counter_) + "." +  compressed_img_msg_.format);
    std::ofstream ofs(image_path);
    ofs.write((char*)compressed_img_msg_.data.data(), compressed_img_msg_.data.size());
    ofs.close();

    snapshot_counter_++;
  }

public:
  LidarCameraSnapshotNode()
  : Node("lidar_camera_snapshot")
  {
    snapshot_dir_ = std::filesystem::current_path() / std::to_string(std::time(NULL));
    std::filesystem::create_directory(snapshot_dir_);

    pointcloud_sub_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
      "/points", rclcpp::SensorDataQoS(), std::bind(&LidarCameraSnapshotNode::pointcloud_callback, this, _1));

    image_sub_ = this->create_subscription<sensor_msgs::msg::CompressedImage>(
      "/color/image/compressed", 10, std::bind(&LidarCameraSnapshotNode::image_callback, this, _1));

    snapshot_srv_ = this->create_service<std_srvs::srv::Empty>(
      "/snapshot", std::bind(&LidarCameraSnapshotNode::save_snapshot, this, _1, _2));

  }
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<LidarCameraSnapshotNode>());
  rclcpp::shutdown();
  return 0;
}
